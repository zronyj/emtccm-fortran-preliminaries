
# Preliminary Exercise 2

Last revision: Oct 19, 2021

---

This program solves the second preliminary exercise of the final project\
for the course *TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Running the program

Before doing anything with the program, please make sure to read the\
general README outside of the folder of this program. It should provide\
details on how to compile all Exercises by using a `make` command.

If that is not possible or if you wish to compile this program as a\
standalone, please check sections 1. a), 1. b) and 1. c) of the general\
README, before running the following command:

`gfortran exercise_2.f90 -o exercise_2.out`

Once you have run this command successfully, you should be able to run\
the program.

### a) Test

The only way to test if this program is working is to actually run it.\
Therefore, I suggest that you try running the following command:

`./exercise_2.out`

The program will prompt you to enter an integer between 1 and 10. If\
this is the case, then the program has run successfully.

### b) Inputs

The program will ask the user to input an integer from 1 to 10. That\
is the only input required by the program.

It should be pointed out that, if the user does not enter a number in\
that range, the program will keep asking for an integer from 1 to 10.

### c) Outputs

This program's output is only displayed on screen. The result will be\
presented as a single line of text.

### d) Theory behind it

The program prompts the user for a number from 1 to 10. It then proceeds\
to compute the square of the number, and the result of adding 30 to the\
number.

Finally, it compares if the number squared is **greater** than, **less**\
than, or **equal** to the same number plus 30.

The result of this comparison is then shown on screen.

## 2. FAQs

1. *Can I run the program in Microsoft Windows?* - If you install Cygwin\
or enable the WSL environment, you should be able to compile and run the\
program. However, we do suggest that you have a look at section 1. a) of\
the general README. GNU Fortran is highly recommended to compile the code.

2. *I deleted an important file. What do I do?* - If you deleted a file,\
please follow this link to locate the project's repository:\
[https://gitlab.com/zronyj/emtccm-fortran-preliminaries](https://gitlab.com/zronyj/emtccm-fortran-preliminaries)

3. *Why does the program evaluate if the square of a number from 1 to 10*\
*can be equal to the number plus 30?* - I do not know. As far as I know,\
no number in this range has this property.