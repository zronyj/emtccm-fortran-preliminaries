program screen_read

implicit none

! -----------------------------------------------------------------------------------------
! This program compares the square of a number with said number plus 30 -------------------
! -----------------------------------------------------------------------------------------

! Define variables
Real :: entrada
Integer :: numero, cuadrado, mastreinta
Logical :: cambio

! Initialize variable to ensure that the user is required to enter a number
cambio = .true.

! While-loop: keep asking the user for an integer between 1 to 10 until his entry is valid
do while(cambio)
	! Politely ask the user for the number
	write(*, "(a)", advance="no") ' Please enter an integer from 1 to 10: '
	! Read the user input from the command line
	read(*, *) entrada
	! Check whether the number is less than 1 or greater than 10
	if ((entrada < 1) .or. (entrada > 10)) then
		! Error message: number out of range
		write(*, *) 'ERROR: The number you entered is not between 1 and 10.'
		! The user is still required to enter a number
		cambio = .true.
	! Check whether the number is an integer
	else if ((entrada - int(entrada))**2 > 1E-8) then
		! Error message: the number is not an integer
		write(*, *) 'The number you entered is not an integer.'
		! The user is still required to enter a number
		cambio = .true.
	! If the number is in range and an integer, then ...
	else
		! Transform the number into an integer
		numero = int(entrada)
		! The user is no longer required to enter a number
		cambio = .false.
	end if
end do

write(*, *) 'The entered number is:', numero

! Number squared
cuadrado = numero**2
! Number plus 30
mastreinta = numero + 30

! Check whether the square of the number is smaller than the number plus 30
if (cuadrado < mastreinta) then
	write(*, *) 'The squared value of this number is smaller than:', mastreinta

! Check whether the square of the number is greater than the number plus 30
else if (cuadrado > mastreinta) then
	write(*, *) 'The squared value of this number is larger than:', mastreinta

! Check whether the square of the number is equal than the number plus 30
else if (cuadrado == mastreinta) then
	write(*, *) 'The squared value of this number is equal to:', mastreinta
end if

end program screen_read