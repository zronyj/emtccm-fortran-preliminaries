program arreglos

implicit none

! -----------------------------------------------------------------------------------------
! This program creates two arrays (a vector and a matrix), initializes, and displays them -
! -----------------------------------------------------------------------------------------

! Define variables
Integer :: i
Integer, dimension(10) :: vec ! Note the dimension when defining
Integer, dimension(3,3) :: mat ! Note the dimension: more dimensions separated by commas

! Initializing the vector
vec(1:3) = 1 ! First 3 entries are equal to 1
vec(4:7) = 2 ! Next 4 entries are equal to 2
vec(8:10) = sum(vec(1:7)) ! Last 3 entries are equal to the sum of all previous entries: 11

! Display the vector as *v =* and the first element of the vector
write(*, 1) 'v =', vec(1) ! Second argument for *write* is the format!
1	format(A4, 1I3) ! Format the output: several outputs may be formated; use commas
write(*, "(1I7)") vec(2:) ! Formatting the output inline requires quotes and brackets
write(*, *) '' ! New line

! Initializing the matrix
mat(1,:) = vec(1:3) ! First column of the matrix equals first 3 elements of the vector
mat(2,:) = 0 ! Second column of the matrix is 0 for all entries
mat(3,:) = vec(8:10) ! Last column of the matrix equals last 3 elements of the vector

! Display the matrix as *mat =* and the first row of the matrix
write(*, 2) 'mat =', mat(:,1)
2	format(A6, 1I2, 2I8) ! Columns from the matrix may be decomposed in the format
write(*, "(3i8)") mat(:,2:)

end program arreglos