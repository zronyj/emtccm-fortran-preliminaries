
# Preliminary Exercise 3

Last revision: Oct 19, 2021

---

This program solves the third preliminary exercise of the final project\
for the course *TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Running the program

Before doing anything with the program, please make sure to read the\
general README outside of the folder of this program. It should provide\
details on how to compile all Exercises by using a `make` command.

If that is not possible or if you wish to compile this program as a\
standalone, please check sections 1. a), 1. b) and 1. c) of the general\
README, before running the following command:

`gfortran exercise_3.f90 -o exercise_3.out`

Once you have run this command successfully, you should be able to run\
the program.

### a) Test

The only way to test if this program is working is to actually run it.\
Therefore, I suggest that you try running the following command:

`./exercise_3.out > exercise_3.log`

This should have stored the output in the file *exercise_3.log*. To check\
for the contents of that file, you may use the command:

`cat exercise_3.log`

If the program ran successfully, then you should see the a vector in\
column representation, and a matrix.

An alternative to this method is to run the program as a standalone. To\
do so, please run only the following command:

`./exercise_3.out`

In this case, the program should have displayed the results on screen for\
you to see them. If this is true, then the program has run successfully.

### b) Inputs

This program does not require any inputs.

### c) Outputs

If you run the program and put the output in a file, as described in\
section 1. a) of this README, then your output should be a file with\
the name *example_3.log*.

Otherwise, if you ran the program as a standalone, then the output will\
be displayed on screen.

### d) Theory behind it

The idea of the program was to create a vector and a matrix using Fortran's\
array construction for integers and real numbers. The dimensions of both\
were predefined from the start.

When initializing them, I took advantage of the auto-fill property of\
arrays. That way, I filled all 3 first entries of the vector, then the\
next 4, and finally the last 3 with just 3 commands.

Following the same logic, the 3x3 matrix was constructed with data from the\
vector for the first and last columns. The middle column was filled with\
zeroes.

Finally, everything was displayed using formatting styles.

## 2. FAQs

1. *Can I run the program in Microsoft Windows?* - If you install Cygwin\
or enable the WSL environment, you should be able to compile and run the\
program. However, we do suggest that you have a look at section 1. a) of\
the general README. GNU Fortran is highly recommended to compile the code.

2. *I deleted an important file. What do I do?* - If you deleted a file,\
please follow this link to locate the project's repository:\
[https://gitlab.com/zronyj/emtccm-fortran-preliminaries](https://gitlab.com/zronyj/emtccm-fortran-preliminaries)