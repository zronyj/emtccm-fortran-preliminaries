#!/bin/bash

if [ -d "./Tests" ]
then
	rm -R Tests
	mkdir Tests
else
	mkdir Tests
fi

# Test for Exercise1
cp ./Exercise1/exercise_1.out ./
if ./exercise_1.out > exercise_1.log
then
	echo -e "\n--------------------------------------------------"
	echo "The test for Exercise1 was successful!"
	echo "--------------------------------------------------"
else
	echo -e "\n--------------------------------------------------"
	echo "ERROR! The test for Exercise1 has FAILED."
	echo "--------------------------------------------------"
fi
mkdir ./Tests/Exercise1
mv exercise_1.log ./Tests/Exercise1/exercise_1.log
rm *.out

# Test for Exercise2
cp ./Exercise2/exercise_2.out ./
if echo "5" | ./exercise_2.out > exercise_2.log
then
	echo -e "\n--------------------------------------------------"
	echo "The test for Exercise2 was successful!"
	echo "--------------------------------------------------"
else
	echo -e "\n--------------------------------------------------"
	echo "ERROR! The test for Exercise2 has FAILED."
	echo "--------------------------------------------------"
fi
mkdir ./Tests/Exercise2
mv exercise_2.log ./Tests/Exercise2/exercise_2.log
rm *.out

# Test for Exercise3
cp ./Exercise3/exercise_3.out ./
if ./exercise_3.out > exercise_3.log
then
	echo -e "\n--------------------------------------------------"
	echo "The test for Exercise3 was successful!"
	echo "--------------------------------------------------"
else
	echo -e "\n--------------------------------------------------"
	echo "ERROR! The test for Exercise3 has FAILED."
	echo "--------------------------------------------------"
fi
mkdir ./Tests/Exercise3
mv exercise_3.log ./Tests/Exercise3/exercise_3.log
rm *.out

# Test for Exercise4
cp ./Exercise4/exercise_4.out ./
if ./exercise_4.out > exercise_4.log
then
	echo -e "\n--------------------------------------------------"
	echo "The test for Exercise4 was successful!"
	echo "--------------------------------------------------"
else
	echo -e "\n--------------------------------------------------"
	echo "ERROR! The test for Exercise4 has FAILED."
	echo "--------------------------------------------------"
fi
mkdir ./Tests/Exercise4
mv exercise_4.log ./Tests/Exercise4/exercise_4.log
mv *.dat ./Tests/Exercise4/
rm *.out

# Test for Exercise5
cp ./Exercise5/exercise_5.out ./
cp ./Exercise5/matrix.dat ./matrix.dat
cp ./Exercise5/vector.dat ./vector.dat
if ./exercise_5.out > exercise_5.log
then
	echo -e "\n--------------------------------------------------"
	echo "The test for Exercise5 was successful!"
	echo "--------------------------------------------------"
else
	echo -e "\n--------------------------------------------------"
	echo "ERROR! The test for Exercise5 has FAILED."
	echo "--------------------------------------------------"
fi
mkdir ./Tests/Exercise5
mv exercise_5.log ./Tests/Exercise5/exercise_5.log
mv *.dat ./Tests/Exercise5/
rm *.out

# Test for Exercise6
cp ./Exercise6/exercise_6.exe ./
cp ./Exercise6/matrix.dat ./matrix.dat
if ./exercise_6.exe 3 matrix.dat > exercise_6.log
then
	echo -e "\n--------------------------------------------------"
	echo "The test for Exercise6 was successful!"
	echo "--------------------------------------------------"
else
	echo -e "\n--------------------------------------------------"
	echo "ERROR! The test for Exercise6 has FAILED."
	echo "--------------------------------------------------"
fi
mkdir ./Tests/Exercise6
mv exercise_6.log ./Tests/Exercise6/exercise_6.log
mv *.dat ./Tests/Exercise6/
rm *.exe