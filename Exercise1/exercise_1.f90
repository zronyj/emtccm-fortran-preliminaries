program factorial

implicit none

! -----------------------------------------------------------------------------------------
! This program computes the factorial of all natural numbers from 1 to 20 -----------------
! -----------------------------------------------------------------------------------------

! Defining variables
Integer:: i, j, n
Integer(kind=8):: fact ! kind=8 is for double precision. It can be higher!

! Initializing variable which will be the last number to compute the factorial of
n = 20

! First do-loop: go from numbers from 1 to 20
do j = 1, n
	! Initialize the value of the result
	fact = 1
	! Second do-loop: advance through all numbers until *n*
	do i = 1, j
		! Multiply the last result with the next number
		fact = fact * i
	end do
	! Write the number *n* and its factorial
	write(6, *) j, '! = ', fact
end do

end program factorial