
# Preliminary Exercise 1

Last revision: Oct 19, 2021

---

This program solves the first preliminary exercise of the final project\
for the course *TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Running the program

Before doing anything with the program, please make sure to read the\
general README outside of the folder of this program. It should provide\
details on how to compile all Exercises by using a `make` command.

If that is not possible or if you wish to compile this program as a\
standalone, please check sections 1. a), 1. b) and 1. c) of the general\
README, before running the following command:

`gfortran exercise_1.f90 -o exercise_1.out`

Once you have run this command successfully, you should be able to run\
the program.

### a) Test

The only way to test if this program is working is to actually run it.\
Therefore, I suggest that you try running the following command:

`./exercise_1.out > exercise_1.log`

This should have stored the output in the file *exercise_1.log*. To check\
for the contents of that file, you may use the command:

`cat exercise_1.log`

If the program ran successfully, then you should see the numbers 1 to 20\
with their computed factorial.

An alternative to this method is to run the program as a standalone. To\
do so, please run only the following command:

`./exercise_1.out`

In this case, the program should have displayed the results on screen for\
you to see them. If this is true, then the program has run successfully.

### b) Inputs

This program does not require any inputs.

### c) Outputs

If you run the program and put the output in a file, as described in\
section 1. a) of this README, then your output should be a file with\
the name *example_1.log*.

Otherwise, if you ran the program as a standalone, then the output will\
be displayed on screen.

### d) Theory behind it

It was asked that the program computed the factorial for each integer\
from 1 to 20, and display the results. The solution was to use two\
nexted `do` loops:

- One for each number
- One for the next element when multiplying the factorial

It should be noted that the precision for this calculation had to be\
incremented to use _double precision_. Otherwise, Fortran would not be\
able to handle such large numbers as the factorial of 20.

## 2. FAQs

1. *Can I run the program in Microsoft Windows?* - If you install Cygwin\
or enable the WSL environment, you should be able to compile and run the\
program. However, we do suggest that you have a look at section 1. a) of\
the general README. GNU Fortran is highly recommended to compile the code.

2. *I deleted an important file. What do I do?* - If you deleted a file,\
please follow this link to locate the project's repository:\
[https://gitlab.com/zronyj/emtccm-fortran-preliminaries](https://gitlab.com/zronyj/emtccm-fortran-preliminaries)

3. *Can I read the saved output with another program?* - The produced\
output was inteded to be shown on screen. Therefore, any other\
manipulation of the output is not suggested. Nevertheless, I do not\
disencourage it.