module matProd_sub ! Notice that this is NOT a program

implicit none

! Establishing that the only subroutine which will be provided to external programs is this
public :: mat_mult

contains ! Begin with the subroutines

	subroutine mat_mult(a, mat, mat2)
	! Special subroutine to multiply all non-diagonal elements of a matrix by a scalar ----
	! Inputs
	! - a: the scalar as a real number
	! - mat: the matrix as a 2D array
	! Outputs
	! - mat2: the matrix after its non-diagonal elements have been multiplied by a
	! Description
	! The subroutine multiplies all non-diagonal elements of a given matrix by a scalar
	! -------------------------------------------------------------------------------------

		implicit none
		
		! Defining variables

		!! The ones staying inside
		integer :: i, j

		! The ones going in and out
		real(kind=8), intent(in) :: a
		real(kind=8), intent(in) :: mat(:,:)
		real(kind=8), allocatable, intent(out) :: mat2(:,:)

		! Assign memory space to the new matrix, based on the size of the previous one
		allocate(mat2(size(mat, 1), size(mat, 2)))

		! Multiplication of matrix with scalar
		do i = 1, size(mat, 1)
			do j = 1, size(mat, 2)
				if (i /= j) then ! If the elements are non-diagonal ...
					mat2(i,j) = mat(i, j) * a
				else ! If they are from the diagonal ...
					mat2(i,j) = mat(i, j)
				end if
			end do
		end do

	end subroutine mat_mult

end module matProd_sub