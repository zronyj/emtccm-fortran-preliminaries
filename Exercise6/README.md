# Preliminary Exercise 6

Last revision: Oct 19, 2021

---

This program solves the sixth preliminary exercise of the final project\
for the course *TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Running the program

Before doing anything with the program, please make sure to read the\
general README outside of the folder of this program. It should provide\
details on how to compile all Exercises by using a `make` command.

If that is not possible or if you wish to compile this program as a\
standalone, please check sections 1. a), 1. b) and 1. c) of the general\
README, before running the following commands:

`gfortran -c exercise_6_sub.f90 -o exercise_6_sub.o`\
`gfortran -c exercise_6.f90 -o exercise_6.o`\
`gfortran exercise_6_sub.o exercise_6.o -o exercise_6.exe`

Once you have run these commands successfully, you should be able to run\
the program.

Please note that this program uses a subroutine from a module. That is\
why the compilation is done in a different manner than the one used for\
the rest of the programs.

The subroutine is the one that multiplies all non-diagonal elements of\
the matrix by a scalar. The main program handles mainly the input and\
output.

### a) Test

To run a quick test of this program, please run the following command by\
making sure that the file _matrix.dat_ is in the same directory as the\
program.

`./exercise_6.exe 3 matrix.dat`

By running this program, you should have seen some output in the screen\
letting you know what the scalar is, and that the matrix was loaded. Then,\
it should mention that the product was carried out and a file was saved.

Additionally, one new file should have been created in the program's\
directory.

### b) Inputs

The program takes 2 kinds of inputs to work. The first kind of input are\
two command line arguments right after the name of the program:

1. A number
2. The name of the file containing the matrix

As soon as the program receives those 2 inputs in that order, it will then\
proceed to take the other kind of input: it will open the file specified as\
argument 2. and it will proceed to read the matrix from the file.

Please note that the matrix doesn't have a particular format; it's just an\
array of numbers arranged as a matrix. For example, a 4x4 identity matrix\
should look like this:

1.0 0.0 0.0 0.0\
0.0 1.0 0.0 0.0\
0.0 0.0 1.0 0.0\
0.0 0.0 0.0 1.0

### c) Outputs

The output of the program will be a file named *mat_multd.dat* containing a\
matrix as an array of numbers in 2D.

### d) Running it

To run the program, please check the inputs specified in section 1. b) of\
this README and use them as follows:

1. Write the name of the program after `./`
2. Write the number you wish to use
3. Write the name of the file containing the matrix

Example:

`./exercise_6.exe 4.15 matrix.dat`

### e) Theory behind it

The program reads a matrix from a file specified as a command line argument,\
and it multiplies its non-diagonal elements by a number also specified as a\
command line argument.

To read the matrix file, the program opens the file and counts the number of\
lines in the file before reading each line as a row of the matrix. Then, to\
count how many columns the matrix has, the program checks for blank spaces\
in the first row.

Once the vector and matrix are read the program performs the multiplication\
by using a double `do` loop followed by an `if` statement.

## 2. FAQs

1. *Can I run the program in Microsoft Windows?* - If you install Cygwin\
or enable the WSL environment, you should be able to compile and run the\
program. However, we do suggest that you have a look at section 1. a) of\
the general README. GNU Fortran is highly recommended to compile the code.

2. *I deleted an important file. What do I do?* - If you deleted a file,\
please follow this link to locate the project's repository:\
[https://gitlab.com/zronyj/emtccm-fortran-preliminaries](https://gitlab.com/zronyj/emtccm-fortran-preliminaries)

3. *May I use any number as an argument?* - The program has been tested\
with several numbers (integers and real), and no problem has arised. If\
you find a problem with this, please let me know.

4. *How big can the matrix be?* - The limit of the vector is the largest\
single-precision number Fortran can handle. In the case of the matrix,\
however, I do not suggest trying with something larger than a 10x10 matrix.
