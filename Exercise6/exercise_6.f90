program main

! Importing the subroutine from the module
use matProd_sub, only : mat_mult

implicit none

! -----------------------------------------------------------------------------------------
! This program multiplies all non-diagonal elements of a matrix by a constant -------------
! -----------------------------------------------------------------------------------------

! Defining variables
integer :: columnas, filas, stat, i, j
character(len=100) :: scalar, filnam, leer, buf, col
real(kind=8) :: my_scalar
real(kind=8), allocatable :: my_mat(:,:), new_mat(:,:)

! Checking if the user provided at least 2 arguments in the command line
if (COMMAND_ARGUMENT_COUNT() /= 2) then
	write(*, *) 'ERROR! scalar and/or name of the file containing the matrix is missing.'
	write(*, *) 'The program should be run: ./matProd.exe c matrix.dat'
	write(*, *) 'Where c is the constant and matrix.dat the name of the file with the matrix.'
	write(*, *) 'Stopping program.'
	stop
end if

write(*, *) 'Beginning with the program ...'
write(*, *) ''

! Saving the values into variables
call GET_COMMAND_ARGUMENT(1,scalar)
call GET_COMMAND_ARGUMENT(2,filnam)

! Parsing the scalar (in text form) to a real number
read(scalar, *) my_scalar

write(*, *) 'You selected ', TRIM(scalar), ' as your scalar number.'
write(*, *) ''

! Opening the file with the matrix
open(44, file=filnam, status='old')

! Read the first line of the file as text
read(44, '(a)') leer

! Read the number of lines in the file to consider them as rows
filas = 1
do
	read(44, *, iostat=stat) buf
	if (stat < 0) exit
	filas = filas + 1
end do

! Read the number of non-consecutive non-space characters from the first row, and consider
! that as the number of columns of the matrix
do i = 1, len(trim(leer))-1
	if (i == 1) then
		if (leer(i:i) /= ' ') then ! If the first character I find is not a blank space ...
			columnas = 1 ! ... then I have my first column!
		else
			columnas = 0 ! Otherwise, I have no column yet.
		end if
	else
		! If the next character is not a space, but it's preceded by a space ...
		if ((leer(i-1:i-1) == ' ') .and. (leer(i:i) /= ' ')) then
			columnas = columnas + 1 ! ... then I have a column!
		end if
	end if
end do

! Assign memory to the matrix and reading it from the file
allocate(my_mat(columnas, filas))
rewind(44)
read(44, *) ((my_mat(i, j), i=1, columnas), j=1, filas)
close(44)

write(*, *) 'Your matrix has been loaded:'
write(*, *) ''
write(col, *) columnas
write(*, "(A4, " // TRIM(col) // "F6.2)") 'M =', my_mat(:,1)
do i=2, filas
	write(*, "('    ', " // TRIM(col) // "F6.2)") my_mat(:,i)
end do
write(*, *) ''

! Calling subroutine by providing the sacalar, the matrix and a variable for the new matrix
call mat_mult(my_scalar, my_mat, new_mat)

! Release memory from the original matrix
deallocate(my_mat)

! Write the new matrix into a new file
open(45, file='mat_multd.dat', status='replace')
do i=1, filas
	write(45, "(" // TRIM(col) // "F8.2)") new_mat(:,i)
end do
close(45)

! Release memory from the new matrix
deallocate(new_mat)

write(*, *) 'The product has been computed and the result has been saved.'

end program main