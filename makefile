# Makefile for Preliminary Exercises

all: exercise_1 exercise_2 exercise_3 exercise_4 exercise_5 exercise_6

exercise_1: ./Exercise1/exercise_1.f90
	gfortran ./Exercise1/exercise_1.f90 -o ./Exercise1/exercise_1.out
	mkdir ./Exercise1/source && mv ./Exercise1/*.f90 ./Exercise1/source/

exercise_2: ./Exercise2/exercise_2.f90
	gfortran ./Exercise2/exercise_2.f90 -o ./Exercise2/exercise_2.out
	mkdir ./Exercise2/source && mv ./Exercise2/*.f90 ./Exercise2/source/

exercise_3: ./Exercise3/exercise_3.f90
	gfortran ./Exercise3/exercise_3.f90 -o ./Exercise3/exercise_3.out
	mkdir ./Exercise3/source && mv ./Exercise3/*.f90 ./Exercise3/source/

exercise_4: ./Exercise4/exercise_4.f90
	gfortran ./Exercise4/exercise_4.f90 -o ./Exercise4/exercise_4.out
	mkdir ./Exercise4/source && mv ./Exercise4/*.f90 ./Exercise4/source/

exercise_5: ./Exercise5/exercise_5.f90
	gfortran ./Exercise5/exercise_5.f90 -o ./Exercise5/exercise_5.out
	mkdir ./Exercise5/source && mv ./Exercise5/*.f90 ./Exercise5/source/

exercise_6: ./Exercise6/exercise_6_sub.o ./Exercise6/exercise_6.o
	gfortran ./Exercise6/exercise_6_sub.o ./Exercise6/exercise_6.o -o ./Exercise6/exercise_6.exe
	mkdir ./Exercise6/source && mv ./Exercise6/*.f90 ./Exercise6/source/
	rm -f ./Exercise6/*.o
	rm -f ./Exercise6/*.mod
	rm -f *.o
	rm -f *.mod
	chmod +x tests.sh

./Exercise6/exercise_6.o: ./Exercise6/exercise_6.f90
	gfortran -c ./Exercise6/exercise_6.f90 -o ./Exercise6/exercise_6.o

./Exercise6/exercise_6_sub.o: ./Exercise6/exercise_6_sub.f90
	gfortran -c ./Exercise6/exercise_6_sub.f90 -o ./Exercise6/exercise_6_sub.o

.PHONY: clean

clean:
	# Exercise1
	mv ./Exercise1/source/*.f90 ./Exercise1/
	rmdir ./Exercise1/source
	rm ./Exercise1/*.out
	# Exercise2
	mv ./Exercise2/source/*.f90 ./Exercise2/
	rmdir ./Exercise2/source
	rm ./Exercise2/*.out
	# Exercise3
	mv ./Exercise3/source/*.f90 ./Exercise3/
	rmdir ./Exercise3/source
	rm ./Exercise3/*.out
	# Exercise4
	mv ./Exercise4/source/*.f90 ./Exercise4/
	rmdir ./Exercise4/source
	rm ./Exercise4/*.out
	# Exercise5
	mv ./Exercise5/source/*.f90 ./Exercise5/
	rmdir ./Exercise5/source
	rm ./Exercise5/*.out
	# Exercise6
	mv ./Exercise6/source/*.f90 ./Exercise6/
	rmdir ./Exercise6/source
	rm ./Exercise6/*.exe
	# Permissions for tests
	chmod -x tests.sh