program vec_mat

implicit none

! -----------------------------------------------------------------------------------------
! This program opens and reads a vector and a matrix from files ---------------------------
! It then computs the vector*vector dot product and the matrix*matrix product -------------
! -----------------------------------------------------------------------------------------


! Define variables
Integer :: numlines, columnas, filas, stat, i, j
character(len=250) :: leer, buf, rows
Real, allocatable :: vec(:), matrix(:,:), res(:,:)

! Starting with the vector ----------------------------------------------------------------
write(*, *) 'Beginning by reading the vector file ...'
write(*, *) ''

! Open the file for the vector
open(44, file='vector.dat', status='old')

! Read the number of lines in the file and consider each one as an entry for the vector
numlines = 0
do
	read(44, *, iostat=stat) buf
	if (stat < 0) exit
	numlines = numlines + 1
end do
rewind(44)

! Allocate space in memory for the vector and read the information to initialize it
allocate(vec(numlines))
read(44, *) vec(:)
close(44)

! User friendliness
write(*, *) 'Vector ready!'
write(*, *) ''
write(*, "(A4, 1F5.2)") 'v =', vec(1)
write(*, "(1F9.2)") vec(2:)
write(*, *) ''

! Open a new file and write the dot product of the vector with itself into it
open(45, file='dotProd.dat', status='replace')
write(45, *) dot_product(vec, vec)
close(45)

write(*, *) 'Dot product computed and result saved.'
write(*, *) ''

! Free the memory assigned to the vector
deallocate(vec)

! Starting with the matrix ----------------------------------------------------------------
write(*, *) 'Reading the matrix file ...'
write(*, *) ''

! Open the file for the matrix
open(46, file='matrix.dat', status='old')

! Read the first line of the file as text
read(46, '(a)') leer

! Read the number of lines in the file to consider them as rows
filas = 1
do
	read(46, *, iostat=stat) buf
	if (stat < 0) exit
	filas = filas + 1
end do

! Read the number of non-consecutive non-space characters from the first row, and consider
! that as the number of columns of the matrix
do i = 1, len(trim(leer))-1
	if (i == 1) then
		if (leer(i:i) /= ' ') then ! If the first character I find is not a blank space ...
			columnas = 1 ! ... then I have my first column!
		else
			columnas = 0 ! Otherwise, I have no column yet.
		end if
	else
		! If the next character is not a space, but it's preceded by a space ...
		if ((leer(i-1:i-1) == ' ') .and. (leer(i:i) /= ' ')) then
			columnas = columnas + 1 ! ... then I have a column!
		end if
	end if
end do

! If the matrix is square, then I can compute the product of the matrix with itself
if (filas .eq. columnas) then

	allocate(matrix(filas, filas))
	rewind(46)
	read(46, *) ((matrix(j,i), i=1, filas), j=1, filas) ! Read the matrix from the file

	! User friendliness
	write(*, *) 'Matrix loaded!'
	write(*, *) ''
	write(rows, *) filas
	write(*, "(A4, " // trim(rows) // "F6.2)") 'M =', matrix(1,:)
	do i=2, filas
		write(*, "('    ', " // trim(rows) // "F6.2)") matrix(i,:)
	end do
	write(*, *) ''

	allocate(res(filas, filas))
	res = matmul(matrix, matrix) ! Perform the matrix-matrix product
	deallocate(matrix)

	! Save the result into a file
	open(47, file='matProd.dat', status='replace')
	do i=1, filas
		write(47, *) res(i,:)
	end do
	close(47)
	deallocate(res)

	write(*, *) 'Matrix-matrix product computed and result saved.'

else ! If the matrix is not squared, then only issue a warning and finish the program
	deallocate(matrix)
	write(*, *) 'WARNING! The provided matrix is not square. Therefore, a product of the'
	write(*, *) 'matrix with itself is not possible.'
end if

close(46) ! Close the file

end program vec_mat