
# Preliminary Exercise 5

Last revision: Oct 19, 2021

---

This program solves the fifth preliminary exercise of the final project\
for the course *TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Running the program

Before doing anything with the program, please make sure to read the\
general README outside of the folder of this program. It should provide\
details on how to compile all Exercises by using a `make` command.

If that is not possible or if you wish to compile this program as a\
standalone, please check sections 1. a), 1. b) and 1. c) of the general\
README, before running the following command:

`gfortran exercise_5.f90 -o exercise_5.out`

Once you have run this command successfully, you should be able to run\
the program.

### a) Test

To run a quick test of this program, please run the following command by\
making sure that the files _matrix.dat_ and _vector.mat_ are in the same\
folder as the program.

`./exercise_5.out`

By running this program, you should have seen some output in the screen\
letting you know that the vector and the matrix were loaded and both\
where subject to multiplication with themselves.

Additionally, two new files should have been created in the program's\
directory.

### b) Inputs

*exercise_5.out* works by reading a **matrix.dat** file and a **vector.dat**\
file. The first one should be a squre matrix with entries as floating point\
numbers. It should not have a header or any additional information; just the\
matrix as is. Each row should be separated by a new line, and each row by a\
space. For example, a 4x4 identity matrix should look like this:

1.0 0.0 0.0 0.0\
0.0 1.0 0.0 0.0\
0.0 0.0 1.0 0.0\
0.0 0.0 0.0 1.0

The vectors should follow the same format considering the fact that they\
are one dimensional matrices. For this reason, a vector in 3D should resemble\
the following:

1.0\
2.1\
3.2

This all means that if you choose to ignore one of these formats, the program\
will give you an error.

### c) Outputs

The program will generate 2 files as outputs: **dotProd.dat** and **matProd.dat**\
The first file will only contain a number, while the second should contain a\
matrix.

The number in **dotProd.dat** is the result of the dot product of the vector\
with itself.

The matrix in **matProd.dat** is the result of the matrix-matrix multiplication\
of the matrix with itself.

### d) Running it

The program can be executed by running the command:

`./exercise_5.out`

What may change the output of the program is if the provided *matrix.dat*\
and *vector.dat* files are different from the ones in the folder for testing\
purposes.

### e) Theory behind it

The idea of this program is to open data files, read them, and perform\
Linear Algebra operations on a vector and a matrix. The operations are\
simple:

- Dot product
- Matrix-Matrix multiplication

There are already built in functions to do that in Fortran. Therefore,\
the most difficult task is to read the vector and the matrix.

The former requires the program to read the number of lines in the file\
*vector.dat* before reading each line as an entry of the vector.

The latter requires the program to carry out the same procedure as with\
the vector, and an additional task: to count how many columns the matrix\
has by checking for blank spaces in the first row.

Once the vector and matrix are read, the matrix is checked to see if it\
is square. Otherwise, the product ca not be computed. Finally, both products\
are computed and the results are saved in two files.

## 2. FAQs

1. *Can I run the program in Microsoft Windows?* - If you install Cygwin\
or enable the WSL environment, you should be able to compile and run the\
program. However, we do suggest that you have a look at section 1. a) of\
the general README. GNU Fortran is highly recommended to compile the code.

2. *I deleted an important file. What do I do?* - If you deleted a file,\
please follow this link to locate the project's repository:\
[https://gitlab.com/zronyj/emtccm-fortran-preliminaries](https://gitlab.com/zronyj/emtccm-fortran-preliminaries)

3. *How big can the vector/matrix be?* - The limit of the vector is the\
largest single-precision number Fortran can handle. In the case of the\
matrix, however, I do not suggest trying with something larger than a\
10x10 matrix.