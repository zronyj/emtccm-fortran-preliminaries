program vectores

implicit none

! -----------------------------------------------------------------------------------------
! This program defines two vectors, makes a sum, and a pointwise product of them ----------
! -----------------------------------------------------------------------------------------

! Define variables
Integer :: i
Real, Parameter :: PI = 3.1415926539 ! Note how to define a constante with *parameter*
Real, dimension(9) :: vec1, vec2, tempvec

write(*, *) 'Creating vectors ...'
vec1 = (/ (i*PI, i=1, 9) /) ! First vector: 9 dimensions, multiples of Pi from 1 to 9
vec2 = (/ (i, i=1, 9) /) ! Second vector: 9 dimensions, multiples from 1 to 9

! Making it a little user-friendly
write(*, *) 'Vectors created!'
1	format(A5, 1F9.6)
write(*, 1) 'v1 =', vec1(1)
write(*, "(1F14.6)") vec1(2:)
write(*, *) ''

write(*, 1) 'v2 =', vec2(1)
write(*, "(1F14.6)") vec2(2:)

write(*, *) 'Sum, product, and saving of the results in file ...'

! Define a format for the output when writing the first line of the vector
2	format(A10, 1F12.6)

! Open a file to write in it: the first number is arbitrary, the status is important
open(44, file='vecs.dat', status='replace')

write(44, 2) 'sum(v1) =', sum(vec1) ! Write the sum of all elements of vector 1
write(44, 2) 'sum(v2) =', sum(vec2) ! Write the sum of all elements of vector 2
write(44, *) '' ! New line

tempvec = vec1 + vec2 ! Sum both vectors
write(44, 2) 'v1 + v2 =', tempvec(1) ! Write the equation and the first entry
write(44, "(1F22.6)") tempvec(2:) ! Write the rest of the vector according to format 1
write(44, *) ''

tempvec = vec1 * vec2 ! Pointwise product of both vectors
write(44, 2) 'v1 * v2 =', tempvec(1) ! Write the equation and the first entry
write(44, "(1F22.6)") tempvec(2:) ! Write the rest of the vector according to format 1

close(44) ! Close file

write(*, *) 'The program has finished successfully.'

end program vectores
