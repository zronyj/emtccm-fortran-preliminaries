
# Preliminary Exercise 4

Last revision: Oct 19, 2021

---

This program solves the fourth preliminary exercise of the final project\
for the course *TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Running the program

Before doing anything with the program, please make sure to read the\
general README outside of the folder of this program. It should provide\
details on how to compile all Exercises by using a `make` command.

If that is not possible or if you wish to compile this program as a\
standalone, please check sections 1. a), 1. b) and 1. c) of the general\
README, before running the following command:

`gfortran exercise_4.f90 -o exercise_4.out`

Once you have run this command successfully, you should be able to run\
the program.

### a) Test

The only way to test if this program is working is to actually run it.\
Therefore, I suggest that you try running the following command:

`./exercise_4.out > exercise_4.log`

This should have stored the output in the file *exercise_4.log*. To check\
for the contents of that file, you may use the command:

`cat exercise_4.log`

If the program ran successfully, then you should see two created vectors\
and the line _ The program has finished successfully._ Additionally, the\
program should have created a new file as an output. If that is the case,\
then the program is running fine.

An alternative to this method is to run the program as a standalone. To\
do so, please run only the following command:

`./exercise_4.out`

In this case, the program should have displayed the messages on screen for\
you to see them, and the output file should be in the same folder as the\
program. If this is true, then the program has run successfully.

### b) Inputs

This program does not require any inputs.

### c) Outputs

If you run the program and put the output in a file, as described in\
section 1. a) of this README, then your output should be a file with\
the name *example_4.log*, and a file with the name *vecs.dat*.

If the program was run as a standalone, then only the file *vecs.dat*\
will have been created in the program's folder.

The file *vecs.dat* should have two vectors in column form:

- 9 entries following the formula $`n (\pi + 1)`$ for $`n=1, 2, ... 9`$
- 9 entries following the formula $`\pi n^2`$ for $`n=1, 2, ... 9`$

### d) Theory behind it

In this exercise, we save two vectors into a file directly using Fortran.

The base vectors are created using an implicit `do` command, while at the\
same time using $`\pi`$ as a constant with the `parameter` keyword.

```math
	v_1 = \left[ \pi, 2 \pi, 3 \pi, 4 \pi, 5 \pi, 6 \pi, 7 \pi, 8 \pi, 9 \pi \right]
```

```math
	v_2 = \left[ 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 \right]
```

Then, the **sum** and the **pointwise product** of $`v_1`$ and $`v_2`$ is carried\
out. The results are 2 new vectors which are saved using the same format\
style used in the previous exercise.

## 2. FAQs

1. *Can I run the program in Microsoft Windows?* - If you install Cygwin\
or enable the WSL environment, you should be able to compile and run the\
program. However, we do suggest that you have a look at section 1. a) of\
the general README. GNU Fortran is highly recommended to compile the code.

2. *I deleted an important file. What do I do?* - If you deleted a file,\
please follow this link to locate the project's repository:\
[https://gitlab.com/zronyj/emtccm-fortran-preliminaries](https://gitlab.com/zronyj/emtccm-fortran-preliminaries)